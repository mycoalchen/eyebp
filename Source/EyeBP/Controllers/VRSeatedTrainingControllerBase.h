// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "VRSeatedTrainingControllerBase.generated.h"

UCLASS()
class EYEBP_API AVRSeatedTrainingControllerBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AVRSeatedTrainingControllerBase();

	UPROPERTY(BlueprintReadWrite)
		class APitchBase* ActivePitch;

	// Always call this implementation fist!
	UFUNCTION()
		virtual void OnPitchThrown(class APitchBase* Pitch);

	// Called once the active pitch goes out of bounds (and we're ready for a new one)
	// Always call this implementation first!
	UFUNCTION()
		virtual void DeactivatePitch();

	class APitcher* Pitcher;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* VRRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		class UCineCameraComponent* BatterCamera;

};
