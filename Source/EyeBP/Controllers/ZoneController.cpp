// Fill out your copyright notice in the Description page of Project Settings.


#include "ZoneController.h"
#include "../Pitches/PitchBase.h"
#include "../UI/ZoneOverlay.h"
#include "Blueprint/UserWidget.h"
#include "CineCameraComponent.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/TextBlock.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"

AZoneController::AZoneController()
{
}

void AZoneController::CreateUI()
{
	if (OverlayClass)
	{
		Overlay = CreateWidget<UZoneOverlay>(GetWorld(), OverlayClass, FName("Overlay"));
		Overlay->AddToViewport();
	}
}

void AZoneController::BeginPlay()
{
	Super::BeginPlay();
	CreateUI();
}

void AZoneController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BatterCamera->CurrentFocalLength += ZoomSensitivity * ZoomInOut;
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::White, FString::SanitizeFloat(BatterCamera->CurrentFocalLength));
}

void AZoneController::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(FName("MouseX"), this, &AZoneController::MouseX);
	PlayerInputComponent->BindAxis(FName("MouseY"), this, &AZoneController::MouseY);
	PlayerInputComponent->BindAction(FName("LMB"), IE_Pressed, this, &AZoneController::LMBDown);
	PlayerInputComponent->BindAction(FName("LMB"), IE_Released, this, &AZoneController::LMBUp);
	PlayerInputComponent->BindAction(FName("RMB"), IE_Pressed, this, &AZoneController::RMBDown);
	PlayerInputComponent->BindAction(FName("RMB"), IE_Released, this, &AZoneController::RMBUp);
}

void AZoneController::DeactivatePitch()
{
	Super::DeactivatePitch();
	// TODO: Overlay stuff
	ActivePitch->Destroy();
	ActivePitch = nullptr;
	bCanSwing = true;
}

void AZoneController::OnPitchThrown(APitchBase* Pitch)
{
	Super::OnPitchThrown(Pitch);
}

void AZoneController::MouseX(float Value)
{
	AddControllerYawInput(LookSensitivity * Value);
}

void AZoneController::MouseY(float Value)
{
	AddControllerPitchInput(-LookSensitivity * Value);
}

void AZoneController::LMBDown()
{
	bLMBHeld = true;
	ZoomInOut = 1;
}
void AZoneController::LMBUp()
{
	bLMBHeld = false;
	if (bRMBHeld) ZoomInOut = -1;
	else ZoomInOut = 0;
}
void AZoneController::RMBDown()
{
	bRMBHeld = true;
	ZoomInOut = -1;
}
void AZoneController::RMBUp()
{
	bRMBHeld = false;
	if (bLMBHeld) ZoomInOut = 1;
	else ZoomInOut = 0;
}
