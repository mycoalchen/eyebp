#pragma once

#include "CoreMinimal.h"
#include "VRSeatedTrainingControllerBase.h"
#include "VRSeatedPrecisionController.generated.h"

UCLASS()
class EYEBP_API AVRSeatedPrecisionController : public AVRSeatedTrainingControllerBase
{
	GENERATED_BODY()

	AVRSeatedPrecisionController();

public:
	virtual void DeactivatePitch() override;

	virtual void OnPitchThrown(class APitchBase* Pitch) override;

	UPROPERTY(BlueprintReadWrite, Category = "Swinging")
		bool bCanSwing = true;
	// Number of seconds to keep swing sphere active
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Swinging")
		float BaseSwingDuration = 0.12;
	UPROPERTY(BlueprintReadWrite, Category = "Swinging")
		float SwingDuration = BaseSwingDuration;

	// Overlay for count, pitch, and swing information
	UPROPERTY(BlueprintReadWrite, Category = "UI")
		class UPrecisionOverlay* Overlay;

	bool IsSwinging() const { return bIsSwinging; }

protected:
	// Sets up reticle and other UI elements; called at beginning
	UFUNCTION()
		void CreateUI();

	// Input handlers
	UFUNCTION()
		void MouseX(float Value);
	UFUNCTION()
		void MouseY(float Value);
	UFUNCTION()
		void LeftClick();
	bool bIsSwinging = false;
	// Called every tick while swing sphere is active
	// Updates location of swing sphere and tracks best swing/ball locations
	UFUNCTION()
		void SwingUpdate();
	// Called when the swing sphere is overlapped
	// Handles hit logic
	UFUNCTION()
		void OnSwingSphereHit(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	// How long the current swing has been active
	float CurrentSwingDuration = 0;
	// Called when swing duration is up
	UFUNCTION()
		void OnSwingFinished();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	// Reticle used to aim at ball
	UPROPERTY(BlueprintReadWrite, Category = "UI")
		class UPrecisionReticle* Reticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		TSubclassOf<class UPrecisionReticle> ReticleClass;
	// Current position of reticle
	UPROPERTY(BlueprintReadWrite, Category = "UI")
		float ReticleX = 0;
	UPROPERTY(BlueprintReadWrite, Category = "UI")
		float ReticleY = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		float ReticleSensitivity = 25;
	float MinReticleX, MinReticleY, MaxReticleX, MaxReticleY;

	// Swing sphere for detecting collision with ball or ball's collision capsule
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Swinging")
		class USphereComponent* SwingSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Swinging")
		float SwingSphereRadius = 17.5;

	// Indicates where the barrel should be swung for various pitches at different heights and y-coordinates
	UPROPERTY(BlueprintReadWrite, Category = "Swinging")
		FPlane SwingPlane;
	// Points defining what the swing plane should be
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Swinging")
		FVector SwingPlanePoint1 = FVector(21.5, -22, 166);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Swinging")
		FVector SwingPlanePoint2 = FVector(10, -10, 130);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Swinging")
		FVector SwingPlanePoint3 = FVector(-21.5, 22, 101);
	// Shortest vector from swing sphere to ball over the course of the swing
	UPROPERTY(BlueprintReadWrite, Category = "Swinging")
		FVector SwingToBall = FVector(10000, 10000, 10000);
	// Swing and ball locations when shortest SwingToBall was calculated
	FVector BestSwingLocation, BestBallLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		class TSubclassOf<class UPrecisionOverlay> OverlayClass;

};
