#include "HeadMountedDisplay.h"
#include "MotionControllerComponent.h"
#include "CinematicCamera/Public/CineCameraComponent.h"
#include "../Pitches/PitchBase.h"
#include "VRSeatedTrainingControllerBase.h"

// Sets default values
AVRSeatedTrainingControllerBase::AVRSeatedTrainingControllerBase()
{
 	PrimaryActorTick.bCanEverTick = true;

	VRRoot = CreateDefaultSubobject<USceneComponent>(TEXT("VR_Root"));
	RootComponent = VRRoot;

	BatterCamera = CreateDefaultSubobject<UCineCameraComponent>(TEXT("BatterCamera"));
	BatterCamera->SetupAttachment(VRRoot);
}

// Called when the game starts or when spawned
void AVRSeatedTrainingControllerBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVRSeatedTrainingControllerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AVRSeatedTrainingControllerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AVRSeatedTrainingControllerBase::OnPitchThrown(APitchBase* Pitch)
{
	ActivePitch = Pitch;
	BatterCamera->FocusSettings.TrackingFocusSettings.ActorToTrack = Pitch;
}

void AVRSeatedTrainingControllerBase::DeactivatePitch()
{
	if (!ActivePitch)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("DeactivatePitch called when ActivePitch was null!"));
		return;
	}
}
