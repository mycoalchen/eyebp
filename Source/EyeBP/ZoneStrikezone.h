// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Strikezone.h"
#include "ZoneStrikezone.generated.h"

/**
 * 
 */
UCLASS()
class EYEBP_API AZoneStrikezone : public AStrikezone
{
	GENERATED_BODY()

		AZoneStrikezone();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	// Called when OverlapBox stops overlapping something
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Current Zone Controller
	class AZoneController* ZoneController;
	// Currently overlapped ball
	class APitchBase* OverlappedBall;
	
	// Input y-z coordinates, output zone (0 is for ball)
	UFUNCTION()
		uint8 GetZone(float y, float z);
	// Checks and sets the zone of the ball
	UFUNCTION()
		void CheckZone(class APitchBase* Ball);
	
public:
	// z and y coordinates of zone edges
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zone dimensions")
		float MidUpperZoneEdge;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zone dimensions")
		float MidLowerZoneEdge;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zone dimensions")
		float MidRightZoneEdge;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zone dimensions")
		float MidLeftZoneEdge;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zone dimensions")
		float StrikezoneRightEdge;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zone dimensions")
		float StrikezoneLeftEdge;
	

};
