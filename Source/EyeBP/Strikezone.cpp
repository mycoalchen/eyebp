// Fill out your copyright notice in the Description page of Project Settings.

#include "Strikezone.h"
#include "Components/BoxComponent.h"
#include "Pitches/PitchBase.h"
#include "UI/PrecisionOverlay.h"
#include "DrawDebugHelpers.h"

// Sets default values
AStrikezone::AStrikezone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	OverlapBox = CreateDefaultSubobject<UBoxComponent>(FName("OverlapBox"));
	if (StrikezoneBottom >= StrikezoneTop)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("Strikezone bottom is above strikezone top!"));
	}
	OverlapBox->SetBoxExtent(FVector(21.59, 21.59, 50 * (StrikezoneTop - StrikezoneBottom)));
	OverlapBox->SetRelativeScale3D(FVector(1, 1, 1));
	OverlapBox->ShapeColor = FColor::Green;
	OverlapBox->SetHiddenInGame(false);
	SuggestedLocation = FVector(-13, 0, 100 * StrikezoneTop - OverlapBox->GetUnscaledBoxExtent().Z * 0.5);
}

// Called when the game starts or when spawned
void AStrikezone::BeginPlay()
{
	Super::BeginPlay();
	OverlapBox->OnComponentBeginOverlap.AddDynamic(this, &AStrikezone::OnOverlapBegin);
}

void AStrikezone::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AStrikezone::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APitchBase* Ball = Cast<APitchBase>(OtherActor);
	if (Ball && Ball->Status == EBallStatus::BS_Ball)
	{
		Ball->Status = EBallStatus::BS_Strike;
	}
}