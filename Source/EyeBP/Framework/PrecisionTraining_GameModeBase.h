// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PrecisionTraining_GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EYEBP_API APrecisionTraining_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APrecisionController* Controller;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APitcher* Pitcher;
	
};
