// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ZoneTraining_GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EYEBP_API AZoneTraining_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class AZoneController* Controller;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APitcher* Pitcher;
};
