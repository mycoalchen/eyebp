// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "EyeBP_GameInstance.generated.h"

UENUM(BlueprintType)
enum class ETrainingMode : uint8
{
	None			UMETA(DisplayName = "None"),
	Precision		UMETA(DisplayName = "Precision (Flat Screen)"),
	Zone			UMETA(DisplayName = "Zone (VR)"),
	Catching		UMETA(DisplayName = "Catching (VR)")
};

UCLASS()
class EYEBP_API UEyeBP_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
		ETrainingMode TrainingMode = ETrainingMode::Precision;
	
};
