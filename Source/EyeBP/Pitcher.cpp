// Fill out your copyright notice in the Description page of Project Settings.


#include "Pitcher.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Pitches/PitchBase.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Controllers/TrainingControllerBase.h"
#include "Controllers/PrecisionController.h"
#include "UI/PrecisionOverlay.h"
#include "Components/CapsuleComponent.h"
#include "CinematicCamera/Public/CineCameraComponent.h"

// Sets default values
APitcher::APitcher()
{
	Capsule = CreateDefaultSubobject<UCapsuleComponent>(FName("Capsule"));
	SetRootComponent(Capsule);
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Body_Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName("BodyMesh"));
	Body_Mesh->SetupAttachment(Capsule);
	ReleasePoint = CreateDefaultSubobject<USceneComponent>(FName("ReleasePoint"));
	ReleasePoint->SetupAttachment(Body_Mesh);
	PitchTarget = CreateDefaultSubobject<USceneComponent>(FName("PitchTarget"));
}

// Called when the game starts or when spawned
void APitcher::BeginPlay()
{
	Super::BeginPlay();

	TrainingController = Cast<ATrainingControllerBase>(GetWorld()->GetFirstPlayerController()->GetCharacter());
	if (TrainingController == nullptr)
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("Controller not found in Pitcher"));

	if (Fastball4Class == nullptr)
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("No fastball class in Pitcher"));
	if (CurveballClass == nullptr)
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("No curveball class in Pitcher"));

	TrainingController->Pitcher = this;
	
}

// Called every frame
void APitcher::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (DrawPitchTarget)
		DrawDebugSphere(GetWorld(), PitchTarget->GetComponentLocation(), 3, 20, FColor::White, false, 2 * DeltaTime);
}

void APitcher::ThrowRandomPitch()
{
	int PitchTypeNum = FMath::RandRange(0, 3);
	EPitchType PitchType = EPitchType(PitchTypeNum);
	int MinMph = MphBounds[PitchTypeNum * 2];
	int MaxMph = MphBounds[PitchTypeNum * 2 + 1];
	int Mph = FMath::RandRange(MinMph, MaxMph);
	// Randomize pitch target location
	float TargetY = FMath::RandRange(-40, 40);
	float TargetZ = FMath::RandRange(5, 150);
	PitchTarget->SetWorldLocation(FVector(0, TargetY, TargetZ));
	BeginThrowPitch(PitchType, Mph, 3000);
}

void APitcher::BeginThrowPitch(EPitchType PitchType, float MPH, float SpinRate)
{
	if (!bCanPitch) return;
	bCanPitch = false;
	FTimerHandle WaitHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("ThrowPitch"), PitchType, MPH, SpinRate);
	PlayPitchAnimation();
	GetWorld()->GetTimerManager().SetTimer(WaitHandle, TimerDelegate, ReleaseDelay, false);

}

void APitcher::ThrowPitch(EPitchType PitchType, float MPH, float SpinRate)
{
	// Spawn the ball at the correct location
	const FActorSpawnParameters SpawnParams;
	const FVector SpawnLocation = ReleasePoint->GetComponentLocation();
	FRotator SpawnRotation; 
	FVector ReleaseToTarget;
	APitchBase* ball;
	switch (PitchType) {
	case EPitchType::PT_Fast4: 
		SpawnRotation = Fastball4StartRotator;
		ReleaseToTarget = (PitchTarget->GetComponentLocation() + FVector(0, 0, 20) - SpawnLocation).GetSafeNormal();
		ball = GetWorld()->SpawnActor<APitchBase>(Fastball4Class, SpawnLocation, SpawnRotation, SpawnParams);
		break;
	case EPitchType::PT_Fast2: 
		SpawnRotation = Fastball2StartRotator;
		ReleaseToTarget = (PitchTarget->GetComponentLocation() + FVector(0, 45, 55) - SpawnLocation).GetSafeNormal();
		ball = GetWorld()->SpawnActor<APitchBase>(Fastball2Class, SpawnLocation, SpawnRotation, SpawnParams);
		break;
	case EPitchType::PT_Curve: 
		SpawnRotation = CurveballStartRotator;
		ReleaseToTarget = (PitchTarget->GetComponentLocation() + FVector(0, 0, 140) - ReleasePoint->GetComponentLocation()).GetSafeNormal();
		ball = GetWorld()->SpawnActor<APitchBase>(CurveballClass, SpawnLocation, SpawnRotation, SpawnParams);
		break;
	case EPitchType::PT_Cut:
		SpawnRotation = CutterStartRotator;
		ReleaseToTarget = (PitchTarget->GetComponentLocation() + FVector(0, -20, 50) - ReleasePoint->GetComponentLocation()).GetSafeNormal();
		ball = GetWorld()->SpawnActor<APitchBase>(CutterClass, SpawnLocation, SpawnRotation, SpawnParams);
		break;
	default:
		return;
		break;
	}
	
	ball->PMC->InitialSpeed = MPH * 44.7;
	ball->PMC->MaxSpeed = 0;
	ball->PMC->Velocity = MPH * 44.7 * ReleaseToTarget;
	ball->SetLifeSpan(5);
	ball->SpinRateRPM = SpinRate;
	ball->SpeedMPH = MPH;
	ball->PitchType = PitchType;

	TrainingController->OnPitchThrown(ball);
}


void APitcher::DeactivatePitch()
{
	TrainingController->DeactivatePitch();
	bCanPitch = true;
}
