// Fill out your copyright notice in the Description page of Project Settings.


#include "ZoneStrikezone.h"
#include "Pitches/PitchBase.h"
#include "Controllers/ZoneController.h"
#include "Components/BoxComponent.h"

AZoneStrikezone::AZoneStrikezone()
{
	StrikezoneRightEdge = StrikezoneWidth * 0.5;
	StrikezoneLeftEdge = -StrikezoneRightEdge;
	MidRightZoneEdge = 0.333 * StrikezoneRightEdge;
	MidLeftZoneEdge = -MidRightZoneEdge;
}

void AZoneStrikezone::BeginPlay()
{
	Super::BeginPlay();
	
	// Define zone edges
	const float CenterZ = GetActorLocation().Z;
	const float Height = OverlapBox->GetUnscaledBoxExtent().Z;
	
	MidUpperZoneEdge = CenterZ + 0.333 * Height;
	MidLowerZoneEdge = CenterZ - 0.333 * Height;

	GEngine->AddOnScreenDebugMessage(-1, 1, FColor::White, FString::SanitizeFloat(MidUpperZoneEdge));
	GEngine->AddOnScreenDebugMessage(-1, 1, FColor::White, FString::SanitizeFloat(MidLowerZoneEdge));

	OverlapBox->OnComponentEndOverlap.AddDynamic(this, &AZoneStrikezone::OnOverlapEnd);

	ZoneController = Cast<AZoneController>(GetWorld()->GetFirstPlayerController()->GetCharacter());
	if (!ZoneController)
		GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Red, TEXT("Using ZoneStrikezone but not ZoneController!"));
}

void AZoneStrikezone::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (OverlappedBall) {
		CheckZone(OverlappedBall);
	}
}

void AZoneStrikezone::CheckZone(APitchBase* Ball)
{
	Ball->Zone = GetZone(Ball->GetActorLocation().Y, Ball->GetActorLocation().Z);
	GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Blue, FString::FromInt(Ball->Zone));
}


uint8 AZoneStrikezone::GetZone(float y, float z)
{
	if (MidUpperZoneEdge < z && z < StrikezoneTop)
	{
		if (MidRightZoneEdge < y && y < StrikezoneRightEdge) return 3;
		if (MidLeftZoneEdge < y && y < MidRightZoneEdge) return 2;
		if (StrikezoneLeftEdge < y && y < MidLeftZoneEdge) return 1;
	}
	else if (MidLowerZoneEdge < z && z < MidUpperZoneEdge)
	{
		if (MidRightZoneEdge < y && y < StrikezoneRightEdge) return 6;
		if (MidLeftZoneEdge < y && y < MidRightZoneEdge) return 5;
		if (StrikezoneLeftEdge < y && y < MidLeftZoneEdge) return 4;
	}
	else if (StrikezoneBottom < z && z < MidLowerZoneEdge)
	{
		if (MidRightZoneEdge < y && y < StrikezoneRightEdge) return 9;
		if (MidLeftZoneEdge < y && y < MidRightZoneEdge) return 8;
		if (StrikezoneLeftEdge < y && y < MidLeftZoneEdge) return 7;
	}
	return 0;
}

void AZoneStrikezone::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	OverlappedBall = Cast<APitchBase>(OtherActor);
	CheckZone(OverlappedBall);
	GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, TEXT("Overlap true"));
}

void AZoneStrikezone::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OverlappedBall == OtherActor)
	{
		OverlappedBall = nullptr;
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, TEXT("Overlap false"));
	}
}
