// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ZoneOverlay.generated.h"


UCLASS()
class EYEBP_API UZoneOverlay : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* AccuracyText; // Cumulative % Accuracy
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* PitchText; // Pitch type and speed
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* StrikeText; // "STRIKE" or "BALL"
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* LocationText;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* UserZoneText; // "Your zone: _"
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ActualZoneText; // "Actual zone: _"
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ActionText; // "Swung" or "Taken"
	// Time to display the parts of the sidebar
	UPROPERTY(BlueprintReadWrite)
		float ZoneDisplayTime = 2;
	UPROPERTY(BlueprintReadWrite)
		float LocationDisplayTime = 2;
	UPROPERTY(BlueprintReadWrite)
		float PitchDisplayTime = 2;
	UPROPERTY(BlueprintReadWrite)
		float StrikeDisplayTime = 1;
	UPROPERTY(BlueprintReadWrite)
		float ActionDisplayTime = 1;
};
