// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PrecisionReticle.generated.h"

UCLASS()
class EYEBP_API UPrecisionReticle : public UUserWidget
{
	GENERATED_BODY()

public:
	UPrecisionReticle(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UImage* ReticleImage;

};
