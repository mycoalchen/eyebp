// Fill out your copyright notice in the Description page of Project Settings.


#include "Fastball4.h"
#include "GameFramework/ProjectileMovementComponent.h"

AFastball4::AFastball4()
{
	SpinRotator = FRotator(1, 0, 0);
	NumBlurMeshes = 10;
	BlurMeshAngle = -5;
	BlurMeshMaxOpacity = 0.8;
}

void AFastball4::BeginPlay()
{
	Super::BeginPlay();
	StartLocation = GetActorLocation();
	PrimaryActorTick.bCanEverTick = true;
	PrevX = GetActorLocation().X;
}

void AFastball4::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	PrevX = GetActorLocation().X;
}