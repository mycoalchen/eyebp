// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PitchBase.h"
#include "Fastball4.generated.h"

UCLASS()
class EYEBP_API AFastball4 : public APitchBase
{
	GENERATED_BODY()

	AFastball4();
	
protected:
	virtual void BeginPlay() override;

	FVector StartLocation;
	float PrevX; // Previous x-coordinate
	
public:

	virtual void Tick(float DeltaTime) override;

};
