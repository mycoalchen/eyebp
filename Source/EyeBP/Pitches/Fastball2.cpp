// Fill out your copyright notice in the Description page of Project Settings.


#include "Fastball2.h"
#include "GameFramework/ProjectileMovementComponent.h"

AFastball2::AFastball2()
{
	// Set SpinRotator, NumBlurMeshes, BlurMeshAngle, and BlurMeshMaxOpacity
	SpinRotator = FRotator(1, 0, 0);
}

void AFastball2::BeginPlay()
{
	Super::BeginPlay();
	StartLocation = GetActorLocation();
	PrimaryActorTick.bCanEverTick = true;
	PrevX = GetActorLocation().X;
}

void AFastball2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	PrevX = GetActorLocation().X;
}