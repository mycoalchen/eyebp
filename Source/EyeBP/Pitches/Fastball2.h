// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PitchBase.h"
#include "Fastball2.generated.h"

UCLASS()
class EYEBP_API AFastball2 : public APitchBase
{
	GENERATED_BODY()

		AFastball2();
	
protected:
	virtual void BeginPlay() override;

	FVector StartLocation;
	float PrevX; // Previous x-coordinate

public:

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Break")
		UCurveFloat* TailCurveFloat; // Maps the tail force at each time

};
