// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PitchBase.h"
#include "Curveball.generated.h"

UCLASS()
class EYEBP_API ACurveball : public APitchBase
{
	GENERATED_BODY()

	ACurveball();
	
protected:
	virtual void BeginPlay() override;

	FVector StartLocation;
	float PrevX; // Previous x-coordinate

public:

	virtual void Tick(float DeltaTime) override;

};
