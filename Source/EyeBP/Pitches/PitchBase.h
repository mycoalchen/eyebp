// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "../Pitcher.h"

UENUM(Blueprintable, BlueprintType)
enum class EBallStatus : uint8
{
	BS_Strike	UMETA(DisplayName = "Strike"),
	BS_Ball		UMETA(DisplayName = "Ball"),
	BS_Hit		UMETA(DisplayName = "Hit"),
};

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PitchBase.generated.h"

UCLASS()
class EYEBP_API APitchBase : public AActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	FVector PrevLocation;

	// Called to stop ball from breaking after being hit
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	// Incremented each time it bounces, to prevent OnHit from being spammed
	int NumBounces = 0;
	
public:
	// Sets default values for this actor's properties
	APitchBase();

	// Release information
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Release Info")
		int SpeedMPH;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Release Info")
		EPitchType PitchType;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
		class UStaticMeshComponent* StaticMeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh")
		class UStaticMesh* BlurStaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		float StaticMeshOpacity = 0.9;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Physics")
		UProjectileMovementComponent* PMC;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Physics")
		FVector MagnusVector = FVector(0, 0, 0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Physics")
		float DragCoefficient = 
		0.5 * 1.225 * 0.509 * 0.0177;
		// 0.5 * rho * Cd * A
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Physics")
		float MagnusCoefficient = 
		0.5 * 1.225 * 0.148 * 0.0177;
		// 0.5 * rho * Cm * A

	// Universal physics tick function for spinning pitches
	UFUNCTION()
		void PhysicsTick();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spin")
		float SpinRateRPM = 2300;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spin")
		FRotator SpinRotator = FRotator(0, 0, 0);

	// Capsule used for collision (ball covers too long a distance every frame)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
		class UCapsuleComponent* CollisionCapsule;
	
	// Array of translucent static meshes used for motion blur
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		TArray<UStaticMeshComponent*> BlurMeshes;
	// Number of blur meshes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		int32 NumBlurMeshes = 20;
	// Angle separating each blur mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		float BlurMeshAngle = -2;
	// Opacity of leading blur mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		float BlurMeshMaxOpacity = 0.3;
	// Translucent ball (white) material used on blur meshes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		class UMaterial* BallTranslucentMaterial;
	// Translucent strap (red) material used on blur meshes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		class UMaterial* StrapTranslucentMaterial;
	// Float curve to use for opacity values - must pass through (1,0) and (0,1) without negative values
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Motion Blur")
		class UCurveFloat* OpacityFloatCurve;

	// Strikezone-determined information
	// Whether this ball is strike, ball, or hit
	EBallStatus Status = EBallStatus::BS_Ball;

	// Used in blueprint
	UFUNCTION(BlueprintCallable)
		EBallStatus GetStatus() const { return Status; }
	
	// Only set when in Zone training mode - 0 is for ball
	uint8 Zone = 0;

	// Keep the ball active for this long
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pitch stats")
		float ActiveDuration = 1;

	// Whether DeactivatePitch has been called on this
	bool bExitCalled = false;
	
	// How long this pitch has been active
	float SecondsActive;

	// Destroy blur meshes
	UFUNCTION(BlueprintCallable)
		void DestroyBlurMeshes();

	UPROPERTY(BlueprintReadWrite)
		bool CallPhysicsTick = true;
	
private:
	
	// Release point of pitcher
	FVector SpawnLocation;
	// Pitcher that threw this ball
	class APitcher* Pitcher;

	// Begin updating the collision capsule after ball has been active for this long (prevents strike bugs)
	float CapsuleUpdateDelay = 0.2f;
};